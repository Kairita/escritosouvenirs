<?php
require '../utils/autoloader.php';

class Modelo
{
    protected $IpBD;
    protected $NombreUsuarioBD;
    protected $PasswordBD;
    protected $NombreBD;
    protected $conexion;
    protected $sentencia;

    public function __construct()
    {
        $this->InicializarConexion();
        $this->conexion = new mysqli(
            $this->IpBD,
            $this->NombreUsuarioBD,
            $this->PasswordBD,
            $this->NombreBD
        );
        if ($this->conexion->connect_error) {
            throw new Exception("¡Error de conexion!");
        }
    }

    protected function InicializarConexion()
    {
        $this->IpBD = IP_BD;
        $this->NombreUsuarioBD = USUARIO_BD;
        $this->PasswordBD = PASSWORD_BD;
        $this->NombreBD = NOMBRE_BD;
    }
}
